'use strict';
var exports = module.exports;
const Auth = require('./auth/user');
const Cons = require('./constructors/core');

exports.handler = function(event, context, cb) {
  const response = (code, res) => {
    // if (res) {
    //   console.log(JSON.stringify(res));
    // }
    cb(null, {
      statusCode: code,
      body: JSON.stringify(res),
      headers: {
        'Content-Type': 'application/json'
      }
    });
  };
  Auth.validateUser(event.headers.Authorization)
    .then(user => {
      switch (event.httpMethod) {
        case 'GET':
          if (user.id && user.email && user.region) {
            Cons.devicesByRegion(user.region)
              .then(devices=>{
                let haveDevices = devices.filter(device=>{return device.devices.length > 0});
                Cons.Meas.getMeasFromDevices(haveDevices)
                  .then(measObjs => {
                    console.log(Array.isArray(measObjs));
                    response(200, measObjs);
                  }, measErr=>{response(measErr.code, measErr.msg)});
              }, deviceErr=>{
                response(deviceErr.code, deviceErr.msg);
              });
          } else {
            let errMsg = 'User credentials incomplete: '
            if (!user.id) {
              errMsg += 'Missing ID property. ';
            }
            if (!user.email) {
              errMsg += 'Missing EMAIL property. ';
            }
            if (!user.region) {
              errMsg += 'Missing REGION property. ';
            }
            response(404, errMsg);
          }
      }
    }, userErr=>{
      response(userErr.code, userErr.msg);
    });
};
