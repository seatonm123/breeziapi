const AWS = require('aws-sdk');
AWS.config.update({region: process.env.REGION || 'eu-west-1'});
const Cognito = new AWS.CognitoIdentityServiceProvider();

const doc = require('dynamodb-doc');
const dynamo = new doc.DynamoDB();

const TABLE_USERS = process.env.TABLE_USERS || 'API_Users';
const ENVIRONMENT_SUFFIX = process.env.ENVIRONMENT_SUFFIX || '-matt';

exports.validateUser = function(jwt){
  const poolData = {
    UserPoolId: process.env.USER_POOL_ID || "eu-west-1_HJnusXcgZ",
    ClientId: process.env.CLIENT_ID || "s589kacic8j2dv5n3jfnvgajc"
  };
  return new Promise((resolve, reject)=>{
    getUserFromToken(jwt)
      .then(user=>{
        getUserFromDB(user)
          .then(dbUser=>{
            resolve(dbUser);
          }, dbErr=>{
            reject(dbErr);
          });
      }, tknErr=>{
        reject(tknErr);
      });
  });
};

function getUserFromToken(jwt){
  return new Promise((resolve, reject)=>{
    Cognito.getUser({
      AccessToken: jwt
    }, (err, data)=>{
      err ?
        reject({code: 404, msg: 'UNAUTHORIZED! That user does not have access to the API'}) :
        resolve(data);
    });
  });
}

function getUserFromDB(userInPool){
  const email = userInPool.UserAttributes.find(att=>att.Name==='email').Value;

  const params = {
    TableName: TABLE_USERS + ENVIRONMENT_SUFFIX
  };
  return new Promise((resolve, reject)=>{
    dynamo.scan(params, (err, users)=>{
      err ?
        reject({code: 404, msg: 'User validated but not found in database'}) :
        resolve(users.Items.find(u=>u.email === email));
    });
  });
}
