const AWS = require('aws-sdk');
const doc = require('dynamodb-doc');
AWS.config.update({region: 'eu-west-1'});
const dynamo = new doc.DynamoDB();

const TABLE_NAME = 'Measurements'
const ENVIRONMENT_SUFFIX = '-integration';

exports.getMeasFromDevices = function(devices){
  return new Promise((resolve, reject)=>{
    let promises = [];
    devices.map(device=>{promises.push(getMeasPerDevice(device))});
    promiseAll(promises)
      .then(measObjs=>{
        resolve(measObjs);
      }, resErr=>{reject({code: 400, msg:'Measurements not found for selected devices'})});
  });
};

function getMeasPerDevice(device){
  return new Promise((resolve, reject)=>{
    let promises = [];
    device.devices.map(d=>{
      promises.push(getOneDeviceMeas(d));
    });
    promiseAll(promises)
      .then(deviceWithMeas=>{
        let mObj = {userId: device.userId, buildingId: device.buildingId};
        mObj.devices = deviceWithMeas;
        resolve(mObj);
      }, mErr=>{reject({code: 400, msg: 'nope'})});
  });
}

function getOneDeviceMeas(dId){
  return new Promise((resolve, reject)=>{
    const params = {
      TableName: TABLE_NAME + ENVIRONMENT_SUFFIX,
      KeyConditionExpression: 'did = :deviceId',
      ExpressionAttributeValues: {':deviceId': dId}
    };
    dynamo.query(params, (err, measurements)=>{
      if (err) {
        reject('couldnt find meases')
      }
      let dObj = {deviceId: dId, measurements: measurements.Items[0]};
      resolve(dObj);
    });
  });
}

function promiseAll(promises) {
  return new Promise((resolve, reject) => {
    const result = [];
    let count = promises.length;
    const checkDone = () => {
      if (--count === 0) resolve(result);
    };
    promises.forEach((promise, i) => {
      promise
        .then(x => {
          result[i] = x;
        }, reject)
        .then(checkDone);
    });
  });
}
