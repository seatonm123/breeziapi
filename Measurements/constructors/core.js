const Users = require('./users');
const Devices = require('./devices');
exports.Meas = require('./measurements');

exports.devicesByRegion = function(region){
  return new Promise((resolve, reject)=>{
    Users.getUsersByRegion(region)
      .then(buildings=>{
        Devices.getDevicesFromRegion(buildings)
          .then(deviceObjs=>{
            resolve(deviceObjs);
          });
      });
  });
};
