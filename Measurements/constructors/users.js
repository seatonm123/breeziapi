const AWS = require('aws-sdk');
const doc = require('dynamodb-doc');
AWS.config.update({region: 'eu-west-1'});
const dynamo = new doc.DynamoDB();

const USER_TABLE_NAME = 'Users';
const BLDG_TABLE_NAME = 'Buildings';
const ENVIRONMENT_SUFFIX = '-integration';

const Region = require('../regions/states');

exports.getUsersByRegion = function(region){
  return new Promise((resolve, reject)=>{
    dynamo.scan({TableName: USER_TABLE_NAME + ENVIRONMENT_SUFFIX}, (err, users)=>{
      if (err) {
        reject(400, 'No users found');
      } else {
        getBldgsForUsers(users)
          .then(bldgs=>{
            getBldgsByRegion(region, bldgs)
              .then(regionBldgs=>{
                resolve(regionBldgs);
              });
          }, bldgErr=>{
            reject(bldgErr);
          });
      }
    });
  });
};

function getBldgsForUsers(users){
  return new Promise((resolve, reject)=>{
    dynamo.scan({TableName: BLDG_TABLE_NAME + ENVIRONMENT_SUFFIX}, (err, bldgs)=>{
      if (err) {
        reject({code: 400, msg: 'No buildings found for selected user group'});
      } else {
        let uIds = [];
        users.Items.map(u=>{uIds.push(u.id)});
        let userBldgs = bldgs.Items.filter(b=>uIds.indexOf(b.userId) !== -1);
        resolve(userBldgs);
      }
    });
  });

}

function getBldgsByRegion(region, buildings){
  return new Promise((resolve, reject)=>{
    resolve(buildings.filter(b=>Region.States[region].indexOf(b.state) !== -1 && b['airVents']));
  });
}
