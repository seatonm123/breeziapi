const Auth = require('./auth/user');
const Cons = require('./constructors/core');

exports.handler = (event, context, cb)=>{
  const response = (code, res)=>{
    // if (res) {
    //   console.log(JSON.stringify(res, null, 1));
    // }
    cb(null, {
      statusCode: code,
      body: JSON.stringify(res),
      headers: {
        'Content-Type': 'application/json'
      }
    });
  };

  Auth.validateUser(event.headers.Authorization)
    .then(user=>{
      switch (event.httpMethod) {
        case 'GET':
          if (user.id && user.email && user.region) {
            Cons.devicesByRegion(user.region)
              .then(devices=>{
                let haveDevices = devices.filter(device=>{return device.devices.length > 0});
                response(200, haveDevices);
              });
          }
      }
    });

};
