const AWS = require('aws-sdk');
AWS.config.update({region: 'eu-west-1'});
const doc = require('dynamodb-doc');
const cognito = new AWS.CognitoIdentityServiceProvider();
const dynamo = new doc.DynamoDB();

const TABLE_USERS = process.env.TABLE_USERS || 'API_Users';
const ENVIRONMENT_SUFFIX = process.env.ENVIRONMENT_SUFFIX || '-matt';

exports.validateUser = function(jwt){
  const poolData = {
    UserPoolId: process.env.USER_POOL_ID || "eu-west-1_HJnusXcgZ",
    ClientId: process.env.CLIENT_ID || "s589kacic8j2dv5n3jfnvgajc"
  };
  return new Promise((resolve, reject)=>{
    getUserFromToken(jwt)
      .then(user=>{
        getUserFromDB(user)
          .then(dbUser=>{
            resolve(dbUser);
          }, dbErr=>{
            reject(dbErr);
          });
      }, tknErr=>{
        reject(tknErr);
      });
  });
};

function getUserFromToken(jwt){
  return new Promise((resolve, reject)=>{
    cognito.getUser({AccessToken: jwt}, (err, user)=>{
      err ?
        reject({code: 400, msg: `This user does not have access to the API`}) :
        resolve(user);
    });
  });
}

function getUserFromDB(userInPool){
  const email = userInPool.UserAttributes.find(att=>att.Name==='email').Value;

  const params = {
    TableName: TABLE_USERS + ENVIRONMENT_SUFFIX
  };
  return new Promise((resolve, reject)=>{
    dynamo.scan(params, (err, users)=>{
      err ?
        reject({code: 404, msg: 'User validated but not found in database'}) :
        resolve(users.Items.find(u=>u.email === email));
    });
  });
}
