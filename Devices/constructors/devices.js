const AWS = require('aws-sdk');
const doc = require('dynamodb-doc');
AWS.config.update({region: 'eu-west-1'});
const dynamo = new doc.DynamoDB();

const TABLE_NAME = 'Devices'
const ENVIRONMENT_SUFFIX = '-integration';

exports.getDevicesFromRegion = function(buildings){
  return new Promise((resolve, reject)=>{
    let promises = [];
    buildings.map(b=>{promises.push(getBldgDevices(b))});
    Promise.all(promises)
      .then(deviceObjs=>{
        resolve(deviceObjs);
      });
  });
};

function getBldgDevices(building){
  return new Promise((resolve, reject)=>{
    dynamo.scan({TableName: TABLE_NAME + ENVIRONMENT_SUFFIX}, (err, devices)=>{
      let returnObj = {userId: building.userId, buildingId: building.id, devices: []};
      if (err) {
        resolve(returnObj);
      } else {
        let rawDevices = devices.Items.filter(device=>device.userId === building.userId);
        rawDevices.map(rD=>{returnObj.devices.push(rD)});
        resolve(returnObj);
      }
    });
  });
}
