const Users = require('./users');
const Device = require('./devices');

exports.devicesByRegion = function(region){
  return new Promise((resolve, reject)=>{
    Users.getUsersByRegion(region)
      .then(users=>{
        Device.getDevicesFromRegion(users)
          .then(devices=>{
            resolve(devices);
          });
      });
  });

};
