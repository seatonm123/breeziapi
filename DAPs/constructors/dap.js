const AWS = require('aws-sdk');
const doc = require('dynamodb-doc');
AWS.config.update({region: 'eu-west-1'});
const dynamo = new doc.DynamoDB();

const TABLE_NAME = 'DataAnalysisPackets';
const ENVIRONMENT_SUFFIX = '-integration';

exports.getDAPsFromRegion = function(users){
  return new Promise((resolve, reject)=>{
    dynamo.scan({TableName: TABLE_NAME + ENVIRONMENT_SUFFIX}, (err, rawDAPs)=>{
      let promises = [];
      users.map(user=>{promises.push(getDAPArr(user, rawDAPs.Items))});
      Promise.all(promises)
        .then(dapObjs=>{
          resolve(dapObjs);
        });
    });
  });
};

function getDAPArr(user, DAPArr){
  return new Promise((resolve, reject)=>{
    let returnObj = {userId: user.userId, buildingId: user.buildingId};
    returnObj.dataAnalysisPackets = DAPArr.filter(dap=>dap.userId === user.userId)[0];
    resolve(returnObj);
  });
}
