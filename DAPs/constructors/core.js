const Users = require('./users');
const DAP = require('./dap');

exports.dapsByRegion = function(region){
  return new Promise((resolve, reject)=>{
    Users.getUsersByRegion(region)
      .then(users=>{
        DAP.getDAPsFromRegion(users)
          .then(daps=>{
            resolve(daps);
          });
      });
  });

};
