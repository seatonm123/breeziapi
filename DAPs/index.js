const Auth = require('./auth/user');
const Cons = require('./constructors/core');

exports.handler = (event, context, cb)=>{
  const response = (code, res)=>{
    // if (res) {
    //   console.log(JSON.stringify(res, null, 1));
    // }
    cb(null, {
      statusCode: code,
      body: JSON.stringify(res),
      headers: {
        'Content-Type': 'application/json'
      }
    });
  };

  Auth.validateUser(event.headers.Authorization)
    .then(user=>{
      switch (event.httpMethod) {
        case 'GET':
          if (user.id && user.email && user.region) {
            Cons.dapsByRegion(user.region)
              .then(daps=>{
                // let haveDevices = devices.filter(device=>{return device.devices.length > 0});
                response(200, daps);
              });
          }
      }
    });

};

const tkn =
  `eyJraWQiOiJXMzZzRyt4V0hzeUllVXpZQzdtemJHUkJUN1FhcTArUjlQZXZ2enRpSFUwPSIsImFsZyI6IlJTMjU2In0.eyJzdWIiOiIxZGFjNTQxZC0wYzhkLTQwNWQtOWUwNy00ZjIwM2M5MjA1YmIiLCJldmVudF9pZCI6ImM1YjUyMDA3LTdhZDMtMTFlOC04YzI5LTAxZGFmNzU4MTE1ZSIsInRva2VuX3VzZSI6ImFjY2VzcyIsInNjb3BlIjoiYXdzLmNvZ25pdG8uc2lnbmluLnVzZXIuYWRtaW4iLCJhdXRoX3RpbWUiOjE1MzAxOTEwNjAsImlzcyI6Imh0dHBzOlwvXC9jb2duaXRvLWlkcC5ldS13ZXN0LTEuYW1hem9uYXdzLmNvbVwvZXUtd2VzdC0xX0hKbnVzWGNnWiIsImV4cCI6MTUzMDE5NDY2MCwiaWF0IjoxNTMwMTkxMDYwLCJqdGkiOiI3NWEwZjAyZC00MGJmLTRjZWYtYmNlMC04ZDUxMjg1YTc0ZGQiLCJjbGllbnRfaWQiOiJzNTg5a2FjaWM4ajJkdjVuM2pmbnZnYWpjIiwidXNlcm5hbWUiOiIxZGFjNTQxZC0wYzhkLTQwNWQtOWUwNy00ZjIwM2M5MjA1YmIifQ.bK2HLfyNkrODjU0kacmusgdGON3fg7eX3gLSqRGg6U_zbYiDYFu0mNJttujxkQaH8gx9xEzpE8df_5x2ggZcgAB8rds_3KtSVVFmJjg3xLfbMVA21oEnwxWeRan7vtJl2Mc4LUmzL4QuZj-s7o7L3BiOx4njOtSqZG54BDLkeA1Sij35Yb6Lin5QuCGuS3hc6sc0Wwbjq8hftg3dB3lgBZAstrngw7bCc59TNrSsdFArt9cXvmOlzUjbzWO7Higd8rnGg8CAwalU8Hyc_naHmn6gHsFZCCAseRyfRQaXv3SEsvjaQckNfBO5oGQ1HFSJaupZxN6mqfdn5uTiu3GFkA`;

const TestEvent = {
  headers: {
    Authorization: tkn
  },
  httpMethod: 'GET'
};

exports.handler(TestEvent, '', (err, data)=>{
  console.log(err, data);
});
