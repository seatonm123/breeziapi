const userApprovedAndConfirmedUrl = 'https://';

$(document).ready(function(){

  $('#submit-button').click(function(){
    let email = $('#email-init').text();
    if (email.length === 0) {
      alert('Please enter your email and hit Submit');
    } else {
      postToUserApprovedConfirmed(email)
        .then(bools=>{
          if (bools.approved === false) {
            alert('You are not authorized to use the Breezi API. Please contact our team for inquiries about becoming a BRAPI User.');
            email = '';
          } else if (bools.confirmed === false) {
            initSignup(email);
          } else if (bools === {approved: true, confirmed: true}) {
            initSignin(email);
          }
        });
    }
  });
});

function postToUserApprovedConfirmed(email){
  return new Promise((resolve, reject)=>{
    if (!email) {
      resolve({approved: false});
    }
    $.post(userApprovedAndConfirmedUrl, (data, response)=>{
      resolve(response);
    });
  });
}
